<?php

$error = [
    'estado' => 1,
    'mensaje' => '',
    'errores' => [],
];

if (empty($_POST['enviar'])) {
    $data = $_POST;
    $res =  Validar($data);
    var_dump($res);
}

function Validar($data =null)
{
    $err = [];
    if (is_array($data)) {
        foreach ($data as $key => $value) {
          
            if ($value == "") {
                $error['estado'] = 0;
                $error['mensaje'] = 'error';
                array_push($err, "{$key} es requerido");
            }
        }

        if (count($err) > 0) {
            $error['errores'] = $err;
        }
    }
    return $error;
    
}
